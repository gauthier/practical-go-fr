== Les commentaires

Avant de passer à des points plus importants, je voudrais passer quelques minutes à parler de commentaires.

[quote, Dave Thomas and Andrew Hunt, The Pragmatic Programmer]
Un bon code a beaucoup de commentaires, un mauvais code nécessite beaucoup de commentaires.

Les commentaires sont très importants pour la lisibilité d'un programme Go. Chaque commentaire doit comporter une et une seule des trois informations suivantes:

. Le commentaire devrait expliquer _quoi_, ce que fait l'objet.
. Le commentaire devrait expliquer _comment_ cela est fait.
. Le commentaire devrait expliquer _pourquoi_, pour quelle raison cela est fait.

La première forme est idéale pour commenter un symbole public:

[source,go]
----
// Open opens the named file for reading.
// If successful, methods on the returned file can be used for reading.
----

La seconde forme est idéale pour commenter une méthode:

[source,go]
----
// queue all dependant actions
var results []chan error
for _, dep := range a.Deps {
        results = append(results, execute(seen, dep))
}
----

La troisième forme, le _pourquoi_, est unique dans la mesure où elle ne déplace pas les deux premières, mais elle ne remplace pas non plus le _quoi_ ou le _comment_ : le style de commentaire _pourquoi_ existe pour expliquer les facteurs externes qui dirige le code que vous lisez sur la page. Souvent, ces facteurs ont rarement un sens pris hors du contexte, le commentaire est là pour fournir ce contexte.

[source,go]
----
return &v2.Cluster_CommonLbConfig{
	// Disable HealthyPanicThreshold
    HealthyPanicThreshold: &envoy_type.Percent{
    	Value: 0,
    },
}
----

Dans cet exemple, les effets de la mise à zéro de la valeur de `HealthyPanicThresold` ne sont peut-être pas clair immédiatement. Le commentaire est nécessaire pour préciser que la valeur 0 désactive le comportement du seuil de panique.

=== Les commentaires sur les variables et les constantes devraient décrire leur contenu et non leur objectif.

Lorsque vous ajoutez un commentaire à une variable ou à une constante, ce commentaire doit décrire le _contenu_ de la variable et non son _objectif_.

[source,go]
----
const randomNumber = 6 // determined from an unbiased die
----

Dans cet exemple, le commentaire explique pourquoi la valeur six est attribuée à `randomNumber`. Le commentaire ne décrit pas l'utilisation où `randomNumber` sera utilisé. Voici d'autres exemples:

[source,go]
----
const (
    StatusContinue           = 100 // RFC 7231, 6.2.1
    StatusSwitchingProtocols = 101 // RFC 7231, 6.2.2
    StatusProcessing         = 102 // RFC 2518, 10.1

    StatusOK                 = 200 // RFC 7231, 6.3.1
----

_Dans le contexte de HTTP_, le code 100 est appelé `StatusContinue`, tel que défini dans la RFC 7231, section 6.2.1.

[TIP]
====
Pour les variables sans valeur initiale, le commentaire doit décrire qui est responsable de l'initialisation de cette variable.

[source,go]
----
// sizeCalculationDisabled indicates whether it is safe
// to calculate Types' widths and alignments. See dowidth.
var sizeCalculationDisabled bool
----

Ici le commentaire indique au lecteur que la fonction `dotwidth` est responsable du maintien de l'état de la variable `sizeCalculationDisabled`.

.Se cacher à la vue de tous
Voici une astuce de Kate Gregory.footnote:[https://www.youtube.com/watch?v=Ic2y6w8lMPA]Parfois, vous trouverez un meilleur nom pour une variable caché dans le commentaire.
[source,go]
----
// registry of SQL drivers
var registry = make(map[string]*sql.Driver)
----

Le commentaire a été ajouté par l'auteur car `registry` ne convient pas suffisamment à définir l'objectif - c'est un registre, mais un registre de quoi?
En renommant la variable `sqlDrivers` il est maintenant clair que le but de cette variable est de contenir les drivers SQL.

[source,go]
----
var sqlDrivers = make(map[string]*sql.Driver)
----
Maintenant, le commentaire est redondant et peut être supprimé.
====

=== Toujours documenter les symboles publics

Puisque godoc est la documentation de votre package, vous devriez toujours ajouter un commentaire pour chaque symbole public - variable, constante, fonction et méthode - déclaré dans votre package.

Voici deux règles du guide de style Google

* Toute fonction publique qui n'est pas à la fois évidente et brève doit être commentée.
* Toute fonction d'une bibliothèque doit être commentée quelle que soit sa longueur ou sa complexité.

[source,go]
----
package ioutil

// ReadAll reads from r until an error or EOF and returns the data it read.
// A successful call returns err == nil, not err == EOF. Because ReadAll is
// defined to read from src until EOF, it does not treat an EOF from Read
// as an error to be reported.
func ReadAll(r io.Reader) ([]byte, error)
----

Il y a une exception à cette règle : vous n'avez pas besoin de documenter les méthodes qui implémentent une interface. En particulier, ne faites pas ça :

[source,go]
----
// Read implements the io.Reader interface
func (r *FileReader) Read(buf []byte) (int, error)
----

Ce commentaire ne dit rien. Il ne vous dit pas ce que fait la méthode, en fait c'est pire, il vous dit d'aller chercher la documentation ailleurs. Dans cette situation, je suggère de supprimer complètement ce commentaire.

Voici un exemple du package io

[source,go]
----
// LimitReader returns a Reader that reads from r
// but stops with EOF after n bytes.
// The underlying implementation is a *LimitedReader.
func LimitReader(r Reader, n int64) Reader { return &LimitedReader{r, n} }

// A LimitedReader reads from R but limits the amount of
// data returned to just N bytes. Each call to Read
// updates N to reflect the new amount remaining.
// Read returns EOF when N <= 0 or when the underlying R returns EOF.
type LimitedReader struct {
    R Reader // underlying reader
    N int64  // max bytes remaining
}

func (l *LimitedReader) Read(p []byte) (n int, err error) {
    if l.N <= 0 {
        return 0, EOF
    }
    if int64(len(p)) > l.N {
        p = p[0:l.N]
    }
    n, err = l.R.Read(p)
    l.N -= int64(n)
    return
}
----

Notez que la déclaration de LimitedReader est directement précédée de la fonction qui l'utilise, et la déclaration de LimitedReader suit la déclaration de LimitedReader elle-même. Même si LimitedReader.Read n'a pas de documentation en soi, il est clair qu'il s'agit d'une implémentation de io.Reader.

[TIP]
Avant d'écrire la fonction, écrivez le commentaire décrivant la fonction. Si vous trouvez difficile d'écrire le commentaire, alors c'est un signe que le code que vous allez écrire va être difficile à comprendre.

